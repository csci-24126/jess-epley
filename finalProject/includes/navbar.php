<?php
include("utils.php");
include("../includes/Recipe.php");
$conn = connect_to_db("finalProjectJessEpley");
session_start();
if (isset($_SESSION['username'])) {
  $userId = getUserId($conn, $_SESSION['username']);
  } else {
  $userId = "";
  }
?>

<head>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
  <link rel="stylesheet" href="../styles/styles.css">  
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>  
</head>

<!-- Bootstrap Navbar-->
<nav class="navbar navbar-expand-lg navbar-dark bg-black">
  <div class="container-fluid">
    <a class="navbar-brand" href="../pages/homepage.php"><img src="../img/H.png">Heirloom</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ms-auto">
      <li class="nav-item">
        <a class="nav-link" href="homepage.php">Home</span></a>
      </li>
      <?php if (isset($_SESSION['username'])) { ?> <!-- start of php if else loop -->
          <li class="nav-item">
            <a class="nav-link" href="../pages/recipeListing.php">My Recipes</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../pages/splash.php">My Profile</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../includes/Logout.php">Logout</a>
          </li>
        <?php } else { ?> <!-- else part of the if-else loop -->
          <li class="nav-item">
            <a class="nav-link" aria-current="page" href="../pages/SignUp.php">Sign Up</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../pages/Login.php">Login</a>
          </li>
        <?php } ?> <!-- closing the if-else loop -->
      </li>
    </ul>
    </div>
  </div>
</nav>
