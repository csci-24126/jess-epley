<div class="container">
  <div class="row">
    <?php
        $data = Recipe::getRecipesFromDb($conn, 9);
        foreach ($data as $recipe) {
    ?>
      <div class="col-12 col-lg-4">
        <a class="card-wrapper"
    href="../pages/recipePage.php?recipeId=<?php echo $recipe->recipeId ?>">
            <div class="card">
                <?php if (!empty($recipe->primaryImage)) { ?>
                  <img class="img-thumbnail" alt ='<?php echo $recipe->imageTitle;?>' src='data:image/jpeg;base64,<?php echo base64_encode( $recipe->primaryImage )?>' />
                <?php } ?>
                <h2><?php echo $recipe->title ?></h2>
                <p><?php echo $recipe->author ?></p>
            </div>
        </a>
      </div>
    <?php
        }
    ?>
  </div>
</div>
