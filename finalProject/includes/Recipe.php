<?php
class Recipe {
    // parameters
    public $conn;
    public $recipeId;
    public $title;
    public $author;
    public $instructions;
    public $publishDate;
    public $primaryImage;
    public $imageTitle;

 
    function __construct($conn, $recipeInfo) {
        $this->conn = $conn;
        $this->recipeId = $recipeInfo['recipeId'];
        $this->title = $recipeInfo['title'];
        $this->author = $recipeInfo['author'];
        $this->instructions = $recipeInfo['instructions'];
        $this->publishDate = $recipeInfo['publishDate'];
        $this->primaryImage = $recipeInfo['primaryImage'];
        $this->imageTitle = $recipeInfo['imageTitle'];
    }
 
    function __destruct() { }

    static function getRecipesFromDb($conn, $numRecipes = 20) {
        $selectRecipes = "SELECT recipes.*, users.fullName AS author
        FROM recipes
        LEFT JOIN users ON users.userId=recipes.author;
        LIMIT :numRecipes";
        $stmt = $conn->prepare($selectRecipes);
        $stmt->bindParam(':numRecipes', $numRecipes, PDO::PARAM_INT);
        $stmt->execute();
       
        $recipeList = array();
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        foreach($stmt->fetchAll() as $listRow) {
            $recipe = new Recipe($conn, $listRow);
            $recipeList[] = $recipe;
        }
     
        return $recipeList;
    }

    static function getRecipesFromDbCurrentUser($conn, $userId) {
        $selectRecipes = "SELECT recipes.*, users.fullName AS author 
        FROM recipes
        LEFT JOIN users ON users.userId=recipes.author
        WHERE userId=:userId";
        $stmt = $conn->prepare($selectRecipes);
        $stmt->bindParam(':userId', $userId, PDO::PARAM_INT);
        $stmt->execute();

        $recipeList = array();
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        foreach($stmt->fetchAll() as $listRow) {
            $recipe = new Recipe($conn, $listRow);
            $recipeList[] = $recipe;
        }
     
        return $recipeList;
    }

    static function getRecipeById($conn, $recipeId) { // Why isn't this pulling the primaryImage / imageTitle??? Shows this select statement is correct in MySQL Workbench
        $selectRecipes = "SELECT recipes.*, users.fullName AS author
        FROM recipes
        LEFT JOIN users ON users.userId=recipes.author
        WHERE recipes.recipeId = :recipeId";

        $stmt = $conn->prepare($selectRecipes);
        $stmt->bindParam(':recipeId', $recipeId, PDO::PARAM_INT);
        $stmt->execute();

        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        foreach($stmt->fetchAll() as $listRow) {
            $recipe = new Recipe($conn, $listRow);
        }

        return $recipe;
    }
    
    function createRecipe() {
        $insert = "INSERT INTO recipes
            (publishDate, title, instructions, author, primaryImage, imageTitle)
            VALUES
            (:publishDate, :title, :instructions, :author, :primaryImage, :imageTitle)";
        $stmt = $this->conn->prepare($insert);
        $stmt->bindParam(':publishDate', $this->publishDate);
        $stmt->bindParam(':title', $this->title);
        $stmt->bindParam(':instructions', $this->instructions);
        $stmt->bindParam(':author', $this->author, PDO::PARAM_INT); // This references a foreign key - integer attached to users table
        $stmt->bindParam(':primaryImage', $this->primaryImage);
        $stmt->bindParam(':imageTitle', $this->imageTitle);
        $stmt->execute();
    }

    function updateRecipe() {
        $currentDate = date('Y-m-d');
        $update = "UPDATE recipes SET
            title=:title,
            instructions=:instructions,
            publishDate=:publishDate,
            primaryImage=:primaryImage,
            imageTitle=:imageTitle
            WHERE recipeId=:recipeId";
        $stmt = $this->conn->prepare($update);
        $stmt->bindParam(':recipeId', $this->recipeId, PDO::PARAM_INT);
        $stmt->bindParam(':title', $this->title);
        $stmt->bindParam(':instructions', $this->instructions);
        $stmt->bindParam(':publishDate', $currentDate);
        $stmt->bindParam(':primaryImage', $this->primaryImage);
        $stmt->bindParam(':imageTitle', $this->imageTitle);
        $stmt->execute();
    }

    function deleteRecipe() {
        $delete = "DELETE FROM recipes WHERE recipeId=:recipeId";
        $stmt = $this->conn->prepare($delete);
        $stmt->bindParam(':recipeId', $this->recipeId, PDO::PARAM_INT);
        $stmt->execute();
    }

} // Closes out Recipe class