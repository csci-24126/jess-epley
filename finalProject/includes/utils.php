<?php

// Clean input
function clean_input($data) {
    $data = trim($data); // removes whitespace
    $data = stripslashes($data); // strips slashes
    $data = htmlspecialchars($data); // replaces html chars
    return $data;
}

// Connect to database
function connect_to_db($dbName) {
    $servername = "localhost";
    $username = "root";
    $password = "";
    try {
        return new PDO("mysql:host=$servername;dbname=$dbName", $username, $password);      
    } catch(PDOException $e) {
        echo "Connection failed: " . $e->getMessage();
    }
}

// Get userId to identify user by their unique primary key ID vs. the username
function getUserId($conn, $username) { 
    $select = "SELECT userId FROM users WHERE username=:username";
    $stmt = $conn->prepare($select);
    $stmt->bindParam(':username', $username);
    $stmt->execute();

    $stmt->setFetchMode(PDO::FETCH_ASSOC);
    foreach($stmt->fetchAll() as $listRow) {
        // print_r($listRow);
        return $listRow['userId'];
    }
}

// Methods to check for username being unique
function checkUsernameIsUnique($username) {
    $conn = connect_to_db("finalProjectJessEpley");
    $selectUser = "SELECT username FROM users WHERE username=:username";
    $stmt = $conn->prepare($selectUser);
    $stmt->bindParam(':username', $username);
    $stmt->execute();
  
    $stmt->setFetchMode(PDO::FETCH_ASSOC);
    return empty($stmt->fetchAll()) ? "" : "Username is already taken";
  }

// Add a new user to the database
function addUser($username, $fullName, $userPassword) {
    $conn = connect_to_db("finalProjectJessEpley");
    $insert = "INSERT INTO users (username, fullName, userPassword)
    VALUES (:username, :fullName, :userPassword)";
    $stmt = $conn->prepare($insert);
    $stmt->bindParam(':username', $username);
    $stmt->bindParam(':fullName', $fullName);
    $stmt->bindParam(':userPassword', $userPassword);
    $stmt->execute();
}

// Method to login user
function login($username) {
    $_SESSION['username'] = $username;
  }  

// Method to validate password entered by user vs. password stored for that username in the database
function verifyPassword($username, $password) {
    $conn = connect_to_db("finalProjectJessEpley");
    $selectUser = "SELECT username, userPassword FROM users WHERE username=:username";
    $stmt = $conn->prepare($selectUser);
    $stmt->bindParam(':username', $username);
    $stmt->execute();
  
    $stmt->setFetchMode(PDO::FETCH_ASSOC);
    foreach($stmt->fetchAll() as $listRow) {
      $hashedPassword = $listRow['userPassword'];
      if (!password_verify($password, $hashedPassword)) {
        return "Incorrect password";
      }  
    }
  }  

// Method to verify if user is in database
function verifyUser($username) {
  $conn = connect_to_db("finalProjectJessEpley");
  $selectUser = "SELECT username FROM users WHERE username=:username";
  $stmt = $conn->prepare($selectUser);
  $stmt->bindParam(':username', $username);
  $stmt->execute();

  $stmt->setFetchMode(PDO::FETCH_ASSOC);
  return empty($stmt->fetchAll()) ? "Username does not exist" : "";
}

// Method to update current user's password
function updateUserPassword($userId, $userPassword) {
    $conn = connect_to_db("finalProjectJessEpley");
    $updatePassword = "UPDATE users
    SET userPassword = :userPassword
    WHERE userId = :userId";
    $stmt = $conn->prepare($updatePassword);
    $stmt->bindParam(':userPassword', $userPassword);
    $stmt->bindParam(':userId', $userId, PDO::PARAM_INT);
    $stmt->execute();
}

function updateUsername($userId, $username) {
    $conn = connect_to_db("finalProjectJessEpley");
    $updateUsername = "UPDATE users 
    SET username = :username
    WHERE userId = :userId";
    $stmt = $conn->prepare($updateUsername);
    $stmt->bindParam(':username', $username);
    $stmt->bindParam(':userId', $userId, PDO::PARAM_INT);
    $stmt->execute();
}

?>