<?php
include("../includes/navbar.php");
$imageErr = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $mbFileSize = $_FILES["fileToUpload"]["size"] / 1000000;
    if ($mbFileSize > 10) {
      $imageErr = "Your file is too large. Max file size is 10MB. Yours was $mbFileSize MB";
    }
    $title = clean_input($_POST["title"]);
    $instructions = clean_input($_POST["instructions"]);
    $primaryImage = file_get_contents($_FILES['fileToUpload']['tmp_name']);
    $imageTitle = htmlspecialchars($_FILES['fileToUpload']['name']);
   
    if (!empty($title) && !empty($instructions)) {
      $author = getUserId($conn, $_SESSION['username']); // Need this to be an integer that links to users table
      $publishDate = date('Y-m-d');
  
      $recipeInfo = array(
        "recipeId" => "",
        "publishDate" => $publishDate,
        "title" => $title,
        "instructions" => $instructions,
        "author" => $author,
        "primaryImage" => $primaryImage,
        "imageTitle" => $imageTitle
      );
  
      $recipe = new Recipe($conn, $recipeInfo);
      $recipe->createRecipe();
      header("Location: RecipeListing.php");
    }
  }
?>
 
<div class="main-container">
  <div class="row justify-content-center">
    <div class="col-md-10 col-lg-8 col-xl-7">
      <form enctype="multipart/form-data" method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        <div class="form-group">
          <label for="title">Recipe Name</label>
          <span class="error">*<br>
          <input type="text" class="form-control" name="title" id="title" required>
        </div>
        <div class="form-group">
          <label for="fileToUpload">Select image to upload:</label>
          <input type="file" name="fileToUpload" id="fileToUpload">
          <span class="error"><?php echo $imageErr;?></span><br>
        </div>
        <div class="form-group">
          <label for="instructions">Instructions</label>
          <span class="error">*</span><br>
          <textarea rows="10" class="form-control" name="instructions" id="instructions" required></textarea>
        </div>
        <input type="submit" class="btn btn-primary" value="Submit">
      </form>    
    </div>
  </div>
</div>

<?php include("../includes/footer.php"); ?>