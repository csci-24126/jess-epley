<?php
include("../includes/navbar.php");
if(!isset($_SESSION['username'])) {
    header("Location: 404.php");
}
?>

<?php
$usernameErr = "";
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $newUsername = clean_input($_POST["newUsername"]);

        if (!empty($newUsername)) {
                $usernameErr = checkUsernameIsUnique($newUsername);
            if (empty($usernameErr)) {
                updateUsername($userId, $newUsername);
                header("Location: splash.php");
            }
        }
    }
?>

<!-- Form for allowing user to change their username -->
<div class="main-container">
    <div class="col-12 col-lg-6 offset-lg-3">
        <h3>Current username: <?php echo $_SESSION['username'];?></h3><br>
        <p>NOTE: You will need to logout and login for change in username to take effect.</p>
    </div>
        <div class="userChangePasswordForm container">
		<div class="row">
			<div class="col-12 col-lg-6 offset-lg-3">
				<form method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]);?>">
					<div class="form-group">
                      <label for="newUsername">Username</label>
                      <span class="error">* <?php echo $usernameErr;?></span><br>
                      <input type="text" class="form-control" name="newUsername" id="newUsername" required>
                    </div>
					<input type="submit" class="btn btn-primary" value="Submit">
                    <a href="splash.php" class="btn btn-primary" style="color: white;">Cancel and return to profile</a>
				</form>
			</div>
		</div>
	</div>
</div>

<?php include("../includes/footer.php"); ?>