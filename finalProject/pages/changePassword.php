<?php
include("../includes/navbar.php");
if(!isset($_SESSION['username'])) {
    header("Location: 404.php");
}
?>

<?php
$passwordErr = "";
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $currentPassword = clean_input($_POST["currentPassword"]);
		$password1 = clean_input($_POST["password1"]);
		$password2 = clean_input($_POST["password2"]);
        $username = $_SESSION['username'];

        if ((!empty($currentPassword)) && (!empty($password1)) && (!empty($password2))) {
            if (!verifyPassword($username, $currentPassword)) {
                $passwordErr = "Password entered doesn't match password in database";
            } else {
                if ($password1 !== $password2) {
                    $userPassword = password_hash($currentPassword, PASSWORD_DEFAULT);
                    $passwordErr = "Passwords must match";
                    } else {
                        $userPassword = password_hash($password1, PASSWORD_DEFAULT);
                    }
            }
        }

		if (empty($passwordErr)) {
            updateUserPassword($userId, $userPassword);
            // header("Location: splash.php");
		}
	}
?>

<!-- Form for checking current password then allowing user to update password -->
<div class="main-container">
	<div class="userChangePasswordForm container">
		<div class="row">
			<div class="col-12 col-lg-6 offset-lg-3">
				<form method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]);?>">
					<div class="form-group">
						<label for="currentPassword">Enter your current password</label>
                        <span class="error">* <?php echo $passwordErr;?></span><br>
                        <input type="password" class="form-control" name="currentPassword" id="currentPassword" required>
					</div>
					<div class="form-group">
						<label for="password1">New Password</label>
                        <span class="error">* <?php echo $passwordErr;?></span><br>
                        <input type="password" class="form-control" name="password1" id="password1" required>
                    </div>
					<div class="form-group">
						<label for="password2">Repeat New Password</label>
						<span class="error">* <?php echo $passwordErr;?></span><br>
						<input type="password" class="form-control" name="password2" id="password2" required>
					</div>
					<input type="submit" class="btn btn-primary" value="Submit">
                    <a href="splash.php" class="btn btn-primary" style="color: white;">Cancel and return to profile</a>
				</form>
			</div>
		</div>
	</div>
</div>

<?php include("../includes/footer.php"); ?>