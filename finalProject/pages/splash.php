<?php
  include("../includes/navbar.php");

  if (isset($_SESSION['username'])) {
?>

<div class='main-container'>
    <div class="row">
        <div class="col-12 col-lg-6 offset-lg-3">
            <h1>Hello, Chef <?php echo $_SESSION['username'] ?>!</h1>
            <ul>
                <li class="profile-button"><a class="btn btn-primary" href="recipeListing.php">Edit / View your Recipes</a></li>
                <li class="profile-button"><a class="btn btn-primary" href="changeUsername.php">Change Username</a><br></li>
                <li class="profile-button"><a class="btn btn-primary" href="changePassword.php">Change Password</a><br></li>
            </ul>
        </div>
    </div>
</div>

<?php
} else {
?>
   
<a href="login.php">Please log in to see this page</a>
<?php } ?>

<?php include("../includes/footer.php"); ?>