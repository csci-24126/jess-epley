<?php
include("../includes/navbar.php");

if (isset($_GET['deleteRecipeId'])) {
    try {
      $recipe = Recipe::getRecipeById($conn, $_GET['deleteRecipeId']);
    } catch(Exception $e) {
      header("Location: RecipeListing.php");
    }
} else {
    header("Location: RecipeListing.php");
    }

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $recipe->deleteRecipe();
    header("Location: RecipeListing.php");
}
?>

<div class="main-container">
  <div class="row justify-content-center">
    <div class="col-md-10 col-lg-8 col-xl-7">
      <form method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        <input type="submit" class="btn btn-danger" value="Permanently Delete Recipe">
      </form>
    </div>
  <div class="col-md-10 col-lg-8 col-xl-7">
      <a href="RecipeListing.php" class="btn btn-success">Cancel and return to Recipe List</a>
  </div>

  <?php include("../includes/footer.php"); ?>