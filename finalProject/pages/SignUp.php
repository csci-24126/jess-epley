<?php
  include("../includes/navbar.php");

  // Checking for errors in user input for username and password before adding to the user's info to the database
  $fullName = $username = $userPassword = "";
  $passwordErr = $usernameErr = "";
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$fullName = clean_input($_POST["fullName"]);
		$username = clean_input($_POST["username"]);
		$password1 = clean_input($_POST["password1"]);
		$password2 = clean_input($_POST["password2"]);

		if ($password1 !== $password2) {
		$userPassword = "";
		$passwordErr = "Passwords must match";
		} else {
			$userPassword = password_hash($password1, PASSWORD_DEFAULT);
		}

		$usernameErr = checkUsernameIsUnique($username); // Checking to make sure username is unique

		if (empty($passwordErr) && empty($usernameErr)) {
		addUser($username, $fullName, $userPassword);
		header("Location: Login.php");
		}
	}
?>

<!-- Form for accepting user's sign up info -->
<div class="main-container">
	<div class="userLoginForm container">
		<div class="row">
			<div class="col-12 col-lg-6 offset-lg-3">
				<form method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]);?>">
					<div class="form-group">
						<label for="fullName">Full Name</label>
						<input type="text" class="form-control" name="fullName" id="fullName">
					</div>
					<div class="form-group">
						<label for="username">Username</label>
						<span class="error">* <?php echo $usernameErr;?></span><br>
						<input type="text" class="form-control" name="username" id="username" required>
					</div>
					<div class="form-group">
						<label for="password1">Password</label>
						<span class="error">* <?php echo $passwordErr;?></span><br>
						<input type="password" class="form-control" name="password1" id="password1">
					</div>
					<div class="form-group">
						<label for="password2">Repeat Password</label>
						<span class="error">* <?php echo $passwordErr;?></span><br>
						<input type="password" class="form-control" name="password2" id="password2">
					</div>
					<input type="submit" class="btn btn-primary" value="Submit">
				</form>
			</div>
		</div>
	</div>
</div>

<?php include("../includes/footer.php"); ?>