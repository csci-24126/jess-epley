<html>
    <title>Heirloom Recipes</title>

<body>
<?php
  include("../includes/navbar.php");
?>
<div class="main-container">
    <div class="jumbotron jumbotron-fluid">
    <div class="container-fluid">

    <!-- Welcome the user to the website: would like to have this dynamically welcome the person once they login by displaying their preferred name -->

    <?php if (isset($_SESSION['username'])) { ?>
        <h1 class='display-3'>Welcome to Heirloom, Chef "<?php echo $_SESSION['username'];?>"!</h1>
        <?php } else { ?>
        <h1 class='display-3'>Welcome to Heirloom!</h1>
        <?php } ?>

        <p class="lead"><strong>Definition of heirloom (er-lüm)</strong>: <br>
    <em><ol>
        <li>a piece of property (such as a deed or charter) that descends to the heir as an inseparable part of an inheritance of real property
        <li>something of special value handed down from one generation to another
        <li>a variety of plant that has originated under cultivation and that has survived for several generations usually due to the efforts of private individuals
    </ol></em></p>
    </div>
    </div>

    <!-- Purpose of the site, need to add more content here so it doesn't look so empty-->
    <div class="jumbotron jumbotron-fluid">
        <div class="container-fluid">
        <p class="lead"><strong>Here you'll be able to collect, search, and contribute your favorite Heirloom recipes. Whether you're looking to compare your favorite recipes
            to how others prepare theirs or looking for a new recipe altogether, you've come to right place. <a href="../pages/SignUp.php">Sign up</a> or <a href="../pages/login.php">login</a> to get started!
        </strong></div>
    </div>

    

    <?php
    include("../includes/carousel.php");
    ?>
    <div class="white-space"><br><br></div> 
    <?php
    include("../includes/3x3Cards.php");
    ?>
    <div class="white-space"><br><br></div>   
</div>

<?php include("../includes/footer.php"); ?>

</body>
</html>

<!-- TODO: Add a "return to recipes" button on recipePage.php
           Utilize the recipe type feature to list simlar recipes in the entire database