<?php
include("../includes/navbar.php");

if ($_GET['recipeId'] != NULL) {
    $recipe = Recipe::getRecipeById($conn, $_GET['recipeId']);
} else {
header("Location: 404.php");
}
?>

<div class="main-container">
    <header class="masthead">
        <div class="container position-relative px-4 px-lg-5">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-md-10 col-lg-8 col-xl-7">
                    <div class="post-heading">
                        <h1><?php echo $recipe->title; ?></h1>
                            <?php if (!empty($recipe->primaryImage)) { ?>
                            <img class= "img-fluid" style = "border-radius: 5px; border: 2px solid black;" alt ='<?php echo $recipe->imageTitle;?>' src='data:image/jpeg;base64,<?php echo base64_encode( $recipe->primaryImage ); ?>' />
                            <?php } ?>
                        <span class="meta">
                            Recipe by <?php echo $recipe->author; ?>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <article class="mb-4">
        <div class="container px-4 px-lg-5">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-md-10 col-lg-8 col-xl-7">
                    <?php echo $recipe->instructions ?>
                </div>
            </div>
        </div>
    </article>
</div>

<?php
include("../includes/carousel.php");
?>

<?php include("../includes/footer.php"); ?>