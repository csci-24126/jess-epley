<?php
include("../includes/navbar.php");
$recipes = Recipe::getRecipesFromDbCurrentUser($conn, $userId);

if(!isset($_SESSION['username'])) {
    header("Location: 404.php");
}
?>
 
<div class="main-container">
    <div class="row">
        <div class="d-flex justify-content-center">
        <a class='btn btn-primary' href='createRecipe.php'>Create New Recipe</a>
        </div>
    </div>

    <?php
      foreach ($recipes as $recipe) { // Would like this to only show recipes created by the current user...
	    $recipeId = $recipe->recipeId;
      ?>
          <div class="row">
            <div class="col-8 col-md-6">
              <a class="" href="recipePage.php?recipeId=<?php echo $recipeId ?>">
                <span class="recipeCRUD"><?php echo $recipe->title ?></span>
              </a>
            </div>
            <div class="col-12 col-md-5 text-end">
              <a class='btn btn-primary btn-sm' href='editRecipe.php?editRecipeId=<?php echo $recipeId ?>'>Edit</a>
              <a class='btn btn-danger btn-sm' href='deleteRecipe.php?deleteRecipeId=<?php echo $recipeId ?>'>Delete</a>
            </div>
          </div>
          <br>
  <?php
    } // Closes our foreach loop
  ?>
</div>

<?php include("../includes/footer.php"); ?>