<?php
include("../includes/navbar.php");
 
$name = $username = $password = "";
$passwordErr = $usernameErr = "";
 
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $username = clean_input($_POST["username"]);
  $password = clean_input($_POST["password"]);

  if (!empty($username) && !empty($password)) {
    $usernameErr = verifyUser($username);
    $passwordErr = verifyPassword($username, $password);
    if (empty($usernameErr) && empty($passwordErr)) {
        login($username);
        header("Location: splash.php");
    }
  }
}
?>


<!-- Form to accept user's login info to check database -->
<div class="main-container">
  <div class='userLoginForm container'>
      <div class="row">
          <div class="col-12 col-lg-6 offset-lg-3">
              <form method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                  <div class="form-group">
                      <label for="username">Username</label>
                      <span class="error">* <?php echo $usernameErr;?></span><br>
                      <input type="text" class="form-control" name="username" id="username" required>
                  </div>
                  <div class="form-group">
                      <label for="password1">Password</label>
                      <span class="error">* <?php echo $passwordErr;?></span><br>
                      <input type="password" class="form-control" name="password" id="password" required>
                  </div>
                  <input type="submit" class="btn btn-primary" value="Submit">
              </form>
          </div>
      </div>
  </div>
</div>

<?php include("../includes/footer.php"); ?>
