<?php
include("../includes/navbar.php");


if (isset($_GET['editRecipeId'])) {
  try {
    $recipe = Recipe::getRecipeById($conn, $_GET['editRecipeId']);
  } catch(Exception $e) {
    header("Location: RecipeListing.php");
  }
} else {
  header("Location: RecipeListing.php");
}

$imageErr = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $mbFileSize = $_FILES["fileToUpload"]["size"] / 1000000;
    if ($mbFileSize > 10) {
      $imageErr = "Your file is too large. Max file size is 10MB. Yours was $mbFileSize MB";
    }

    $title = clean_input($_POST["title"]);
    $instructions = clean_input($_POST["instructions"]);
    $primaryImage = file_get_contents($_FILES['fileToUpload']['tmp_name']);
    $imageTitle = htmlspecialchars($_FILES['fileToUpload']['name']);

    if (!empty($title) && !empty($instructions)) {
        $recipe->title = $title;
        $recipe->instructions = $instructions;
        $recipe->primaryImage = $primaryImage;
        $recipe->imageTitle = $imageTitle;
        $recipe->updateRecipe();
        header("Location: RecipeListing.php");   
    }
} // Closes our first "if" statement
  
?>
 
<div class="main-container">
  <div class="row justify-content-center">
    <div class="col-md-10 col-lg-8 col-xl-7">
      <form enctype="multipart/form-data" method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        <div class="form-group">
          <label for="author">Author</label>
          <span class="error">*<br>
          <input type="text" class="form-control" name="author" id="author" disabled value="<?php echo $recipe->author ?>"required>
        </div>
        <div class="form-group">
          <label for="title">Recipe Name</label>
          <span class="error">*<br>
          <input type="text" class="form-control" name="title" id="title" value="<?php echo $recipe->title ?>" required>
        </div>
        <div class="row">
          <div class="col-9 col-md-6">
            <div class="form-group">
                <label for="fileToUpload">Select image to upload:</label>
                <input type="file" name="fileToUpload" id="fileToUpload">
                <span class="error"><?php echo $imageErr;?></span><br>
            </div>
          </div> <!-- closes div col-9 col-md-6 -->
          <div class="col-3 col-md-6">
            <?php if (!empty($recipe->primaryImage)) { ?>
              <img class="d-block w-100" alt ='<?php echo $recipe->imageTitle;?>' src='data:image/jpeg;base64,<?php echo base64_encode( $recipe->primaryImage )?>' />
            <?php } ?>
          </div> <!-- closes div col-3 col-md-6 -->
        </div> <!-- closes div row for image upload -->
        <div class="form-group">
          <label for="instructions">Instructions</label>
          <span class="error">*</span><br>
          <textarea rows="10" class="form-control" name="instructions" id="instructions" required><?php echo $recipe->instructions ?></textarea>
        </div>
        <input type="submit" class="btn btn-primary" value="Submit">
    </form>    
    </div>
  </div>
</div>

<?php include("../includes/footer.php"); ?>
