-- DROP DATABASE finalProjectJessEpley;
CREATE DATABASE finalProjectJessEpley;
USE finalProjectJessEpley;
DROP TABLE IF EXISTS foodtypes;
CREATE TABLE foodtypes (
	cuisineId INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    cuisine VARCHAR(255)
    );

DROP TABLE IF EXISTS users;
CREATE TABLE users (
	userId INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    username VARCHAR(255),
    fullName VARCHAR(255),
    userPassword VARCHAR(255)
    );

DROP TABLE IF EXISTS recipes;
CREATE TABLE recipes (
	recipeId INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    publishDate DATE,
    title VARCHAR(255),
    instructions VARCHAR(8000),
    category INT,
    author INT,
    primaryImage MEDIUMBLOB,
    imageTitle VARCHAR(255),
    FOREIGN KEY (category) REFERENCES foodtypes(cuisineId),
    FOREIGN KEY (author) REFERENCES users(userId)
    );
    

INSERT INTO foodtypes (cuisine)
VALUES
("italian"),
("mexican"),
("chinese"),
("mediterranean"),
("american"),
("other");

-- INSERT INTO users (email, fullName, userPassword, userAuth)
-- VALUES
-- ("jessica@jessica.com", "Jess Epley", "insecurepassword", 1),
-- ("jason@jason.com", "Jason Frank", "reallyinsecurePW", 2),
-- ("yourmom@mom.com", "Your Mom", "thatsfunny", 3);

-- INSERT INTO recipes (title, instructions, category, author)
-- VALUES
-- ("Spaghetti", "Boil noodles, heat sauce and meatballs, enjoy!", 1, 1),
-- ("Tacos", "Cook the meat, chop up lettuce and tomatoes, serve with salsa and any other fixin's you'd like!", 2, 2),
-- ("Cheeseburger", "Cook meat patty, toast bun, add condiments, enjoy!", 5, 3);
