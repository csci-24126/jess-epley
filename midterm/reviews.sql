CREATE DATABASE midtermJess;
USE midtermJess;
 
DROP TABLE IF EXISTS Reviews;
CREATE TABLE Reviews (
  reviewId INT PRIMARY KEY AUTO_INCREMENT,
  reviewText VARCHAR(255), 
  numStars INT
);
 
INSERT INTO Reviews (reviewText, numStars)
VALUES
("Killer Clowns from Outer Space", 1),
("Jumanji", 5);

SELECT * FROM Reviews WHERE numStars < 3;