<?php
include("../cms/includes/navbar.php"); // Adding bootstrap navbar which also includes utils.php from cms folder
?>

<!-- Referencing our stylesheet to make our webpage look nice -->
<link rel="stylesheet" href="reviews.css">

<!-- HTML code for our movie reviews form -->
<form method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]);?>">
<label for="reviewText"><strong>Write your movie review (be sure to include movie title):</strong></label>
<input type="text" name="reviewText" id="reviewText" required><span class="error"> *</span><br><br>

<!-- Radio buttons for star ratings -->
<p><strong>Select the rating you'd give the movie (1-5 stars): </strong><span class="error"> *</span></p> 
    <input type="radio" name="starRating" id="oneStar" value="1" checked>
    <label for="oneStar">★ (lowest rating)</label><br>
    <input type="radio" name="starRating" id="twoStar" value="2">
    <label for="twoStar">★★</label><br>
    <input type="radio" name="starRating" id="threeStar" value="3">
    <label for="threeStar">★★★</label><br>
    <input type="radio" name="starRating" id="fourStar" value="4">
    <label for="fourStar">★★★★</label><br>
    <input type="radio" name="starRating" id="fiveStar" value="5">
    <label for="twoStar">★★★★★ (highest rating)</label><br>

<input type="submit" class="btn btn-primary" value="Submit">
<br><br> <span class="error"> * denotes required field </span>
</form>

<?php
// Set variables that'll be used in the form
$reviewText = "";
$starRating = "";

// Catching empty review text
$emptyReview = "";

/* Accepting user input from the form and cleaning input 
   (adding a check for user not adding text - but isn't this redundant since it's a required field?) */
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST['reviewText'])) {
        $emptyReview = "Please enter your review!";
    } else {
        $reviewText = clean_input($_POST['reviewText']);
    }

    if (isset($_POST['starRating'])) {
        $starRating = $_POST['starRating'];
    }
}



// Connect to our SQL database
$conn = connect_to_db("midtermJess");

// Calling our function to add user's movie review to our database
if (!empty($reviewText)) {
    addMovieReview($conn, $reviewText, $starRating);
}

// Allow user to delete their movie review
if(isset($_GET['deletedItemId'])) {
    deleteMovieReview($conn, $_GET['deletedItemId']);
}
?>

<!-- HTML coding to stylize the movie review results -->
<h2>Movie reviews:</h2><br><br>

<?php
// Function called to print the to do list
printMovieReview($conn);


// FUNCTIONS FOR INTEGRATING SQL AND PHP / CRUD OPERATIONS

// Function to add an item to the movie review database
function addMovieReview($conn, $reviewText, $starRating) {
    $addReview = "INSERT INTO Reviews (reviewText, numStars)
    VALUES (:reviewText, :numStars)";
    $stmt = $conn->prepare($addReview);
    $stmt->bindParam(':reviewText', $reviewText);
    $stmt->bindParam(':numStars', $starRating);
    $stmt->execute();
}

// Function for deleting a movie review from the database
function deleteMovieReview($conn, $reviewId) {
    $delete = "DELETE FROM Reviews WHERE reviewId=:reviewId";
    $stmt = $conn->prepare($delete);
    $stmt->bindParam(':reviewId', $reviewId);
    $stmt->execute();
}

// Function to print out the Movie Review
function printMovieReview($conn) {
    $selectItem = "SELECT * FROM Reviews";
    $stmt = $conn->prepare($selectItem);
    $stmt->execute();
 
    $stmt->setFetchMode(PDO::FETCH_ASSOC);
    foreach($stmt->fetchAll() as $listRow) {
        echo "<div class='movieReview row'>";
        $reviewId = $listRow['reviewId'];
        $movieReview = $listRow['reviewText'];
        $starRating = $listRow['numStars'];
        echo "<p class='col-3 offset-3'>$movieReview</p>";
        if ($starRating === 1) {
            echo "<br><p class='col-2'>★</p>
            <a class='btn btn-danger offset-1 col-1' href='reviews.php?deletedItemId=$reviewId'>Delete</a> </br>";
        } elseif ($starRating === 2) {
            echo "<br><p class='col-2'>★★</p>
            <a class='btn btn-danger offset-1 col-1' href='reviews.php?deletedItemId=$reviewId'>Delete</a> </br>";
        } elseif ($starRating === 3) {
            echo "<br><p class='col-2'>★★★</p>
            <a class='btn btn-danger offset-1 col-1' href='reviews.php?deletedItemId=$reviewId'>Delete</a> </br>";
        }  elseif ($starRating === 4) {
            echo "<br><p class='col-2'>★★★★</p>
            <a class='btn btn-danger offset-1 col-1' href='reviews.php?deletedItemId=$reviewId'>Delete</a> </br>";
        }  else {
            echo "<br><p class='col-2'>★★★★★</p>
            <a class='btn btn-danger offset-1 col-1' href='reviews.php?deletedItemId=$reviewId'>Delete</a> </br>";
        }  
        echo "</div>";
    }
}

// end file