<?php
/* 
There are at least 8 things that should be changed about this code. 
Some of them will prevent the program from running,
others are stylistic/good coding practices.
The output you should see in your browser is 
"Ludwig van Beethoven has a famous song called Ode to Joy"
Hint: Double check that your if statement works by changing the value for $x
*/

$artistName= "Ludwig van Beethoven";
$songName = "Ode to Joy";

// Check to see if the artist is Ludwig van Beethoven
if ($artistName === "Ludwig van Beethoven") {
echo "$artistName has a famous song called $songName";
} 

// Print an alternative message if the artist isn't Ludwig van Beethoven
else {
    echo "$artistName doesn't have a famous song called $songName";
}

?>