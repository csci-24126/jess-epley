<?php

// First function for multiplying by 37.

function multiplyBy37($usernum) {
    if (!is_numeric($usernum)) {
        throw new Exception("$usernum is not a number!");
    }
    $multiBy37 = $usernum * 37;
    echo ($usernum . " * 37 = " . $multiBy37 . "</br>");
    return $multiBy37;
}

// Testing output of function
$var1 = multiplyBy37(2);
multiplyBy37(3);
multiplyBy37(0);
echo "$var1 </br>";

// Try catch block for exception handling
try {
    multiplyBy37("mayonnaise");
} catch (Exception $ex) {
    $message = $ex->getMessage();
    $file = $ex->getFile();
    $line = $ex->getTraceAsString();
    echo "$message </br> $file </br> $line </br>";
}



// Puzzle section - I decided to us an array to hold the numbers we want to sum
echo "</br>";
function naturalNums($topBoundary) {
    if (!is_numeric($topBoundary)) { // Throwing for non-numeric parameters.
        throw new Exception("$topBoundary is not a number!");
    }
    $sumTheseNums = array();
    for ($i = 1; $i < $topBoundary; $i++) {
        if ($i % 3 == 0 || $i % 5 == 0) { // Checking if number is divisible by 3 or 5
            array_push($sumTheseNums, $i); // If TRUE, push that number to the array and repeat the loop
        }
    }
    return array_sum($sumTheseNums); // Sum the numbers pushed to the array
}

// try {
//     naturalNums("Mom");
// } catch (Exception $ex) {
//     $message = $ex->getMessage();
//     $file = $ex->getFile();
//     $line = $ex->getTraceAsString();
//     echo "$message </br> $file </br> $line </br>";
// }

echo (naturalNums(10) . "</br>");
echo (naturalNums(100) . "</br>");
echo (naturalNums(1000) . "</br>");
echo (naturalNums(10000) . "</br>");
echo (naturalNums(7) . "</br>");
echo (naturalNums(0) . "</br>");
echo (naturalNums("Jess"));

//End DIY section
