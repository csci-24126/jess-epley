<?php

// ARRAYS

$starWarsFilms = array("A New Hope", "The Empire Strikes Back", "Return of the Jedi");
print_r($starWarsFilms);
echo ('<br>');

echo ('<br>' . $starWarsFilms[1] . '</br>'); // Printing "The Empire Strikes Back" from our array
echo ('<br>' . count($starWarsFilms) . '</br>'); // Prints how many elements are in the array
sort($starWarsFilms); // Sorts the array alphabetically

array_push($starWarsFilms, "The Phantom Menace"); // Adds an element at the end of the array
unset ($starWarsFilms[0]); // Removes element at index 0 from the array

$myTopFiveGames = array("Civilization", "Age of Empires", "Sim City", "Diablo", "League of Legends"); // An array with my top 5 favorite games

$speciesOfMyPets = array("Morty"=>"Italian Greyhound", "Lady"=>"Italian Greyhound", "Lizzy"=>"Bearded Dragon");
echo ('<br>' . $speciesOfMyPets["Lady"] . '</br>'); // Printing the value associated with Lady


// LOOPS

$i = 0;
while ($i <= 5) {
    echo "The current number is $i </br>";
    $i++;
}

echo ('<br>');
for ($i = 0; $i <= 5; $i++) {
    echo "The current number is $i </br>";
}

echo ('<br>');
for ($i = -52; $i <= 100; $i += 5) {
    echo "The current number $i </br>";
}

// My Sleepy time sheep loop
echo ('<br>');
for ($i = 1; $i <= 20; $i++) {
    echo "$i sheep lay on the pillow and fall asleep - ZzzzZzzZz... </br>";
}

// Foreach loops
echo ('<br>');
$avengers = array("Thor", "Captain America", "Iron Man", "Black Widow");

foreach ($avengers as $hero) {
    echo "$hero is in the Avengers </br>";
}

// Printing my top 5 array from above
echo ('<br>');
foreach ($myTopFiveGames as $game) {
    echo "$game is one of my favorite games </br>";
}

// FUNCTIONS
echo ('<br>');
function echoHelloWorld() {
    echo "Hello World </br>";
}

echoHelloWorld(); // Calling the function we just made
echoHelloWorld();

echo ('<br>');
function echoName($name) {
    echo "Hello $name </br>";
}

echoName("Jess");
echoName("Jason");

// Using Number Test as a function using last lab's DIY code
echo ('<br>');
function checkDivisibleBy3or5($numberToTest) {
    if ($numberToTest % 5 == 0 && $numberToTest % 3 == 0) {
        echo "divisible by both 3 & 5 </br>";
    } elseif ($numberToTest % 5 == 0) {
        echo "divisible by 5 </br>";
    } elseif ($numberToTest % 3 == 0) {
        echo "divisible by 3 </br>";
    } else {
        echo "not divisible by 3 or 5 </br>";
    }
}

checkDivisibleBy3or5(3);
checkDivisibleBy3or5(5);
checkDivisibleBy3or5(9);
checkDivisibleBy3or5(10);
checkDivisibleBy3or5(15);
checkDivisibleBy3or5(19);

echo ('<br>');
function cube($numToCube) {
    $cube = $numToCube * $numToCube * $numToCube;
    return $cube;
}

$cubeOf3 = cube(3); // Assigning function's returned value to the variable
$cubeOf5 = cube(5);
echo ($cubeOf3 . '<br>'); // printing value of those variables
echo ($cubeOf5 . '<br>');
echo ($cubeOf3 * $cubeOf5 . '<br>');


// EXCEPTION HANDLING
echo ('<br>');
function divide($dividend, $divisor) {
    if ($divisor == 0) {
        throw new Exception("Division by zero");
    }
    return $dividend / $divisor;
}

try {
    echo divide(5, 0);
} catch (Exception $ex) {
    $message = $ex->getMessage();
    $file = $ex->getFile();
    $line = $ex->getTraceAsString();
    echo "$message $file $line . </br>";
} finally {
    echo "Process complete. </br";
}

// End guided lab section