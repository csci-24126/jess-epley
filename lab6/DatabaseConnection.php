<?php
$servername = "localhost";
$username = "root";
$password = "";

try {
  $conn = new PDO("mysql:host=$servername;dbname=lab6", $username, $password);
  // echo "Connected successfully";
  $olympians = "SELECT * FROM Olympians"; // Select statement to run mySQL query
  $stmt = $conn->prepare($olympians); // Prepare our connection for security measures
  $stmt->execute(); // Execute the prepared statement

  $stmt->setFetchMode(PDO::FETCH_ASSOC);
//   print_r($stmt->fetchAll()); // Displaying results

	foreach($stmt->fetchAll() as $allOlympians) {
			echo "<br>";
		foreach($allOlympians as $olympian) {
			echo "$olympian <br>";
		}
	}

} catch(PDOException $e) {
  echo "Connection failed: " . $e->getMessage();
}
