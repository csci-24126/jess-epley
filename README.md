# Lab 1 - Jess Epley
This lab is an introduction to using git and bash.
## Big git no-nos
Never commit directly to the main branch in a repo
## Merge Conflicts
This is from merge-conflict-part-1

# About me
I'm Jess, short for Jessica, but you can call me whichever. I'm attending CSCC to switch careers into web development. My hobbies include playing videogames
notably League of Legends and Diablo 3. I have two italian greyhounds - Lady and Morty. I enjoy problem solving while using the creativity side of my brain 
which is why I've found enjoyment in learning web development. Looking forward to learning more in this course!

# Proposed final project
For my project, I have plenty of ideas swirling. My initial goal is to have a "recipe tracker" where a user can create a login and upload recipes they've created or made
that they'd recommend to others. I'd like them to be able to add a title to the recipe, ingredients / measurements, and step-by-step instructions. On a more in-depth level beyond this,
I'd like to have a searchable database of recipes compiled by users where they could select recipes based on a main ingredient (ie: chicken, beef, etc.) or the length of time
the recipe takes (ie: recipes under 30 minutes, recipes using less than 10 ingredients). I feel it could be nice for those of us that have limited time to decide on meals, or
are wholly indecisive on what we want to eat!

The more users that sign up and add to their personal databases, the more functionality availabile to subscribers.

Some challenges I foresee with this will be if it's too bulky or complex to actually accomplish. I'm unsure if we're going to have "searchable" functionality to our websites, but
something to be mindful of. Also, if users format their recipes differently, this could results in the searchability functions not working as intended (having a "tags" feature may help with this.)

Let me know if this is a bit ambitious and we could just start with the "enter recipes to your profile" concept :)
