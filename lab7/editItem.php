<?php
include("../cms/includes/navbar.php");
?>

<link rel="stylesheet" href="toDoList.css">

<?php
// Connecting to database
$conn = connect_to_db("toDoList");
?>

<!-- HTML code for our to do list form -->
<div class='toDoList'>
  <form method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]);?>">
    <label for="toDo">To Do:</label>
    <input type="text" name="toDo" id="toDo" value="<?php echo $toDoItem = getValueOfToDoItem($conn, $_GET['editItemId']); ?>"><br> <!-- Need to figure out this autofill item -->

    <input type="checkbox" name="isDone" id="isDone" value="true">
    <label for="isDone">Is Done</label>

    <input type="submit" class="btn btn-primary" value="Submit">
  </form>

<?php



// Setting variable in preparation for user's decision if item is done or not
$isDone = false;

// Accepting user input from the form
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (clean_input($_POST['toDo'])) {
        $toDoItem = clean_input($_POST['toDo']);
    }
    if (isset($_POST['isDone'])) {
        $isDone = true;
    }
    if (isset($_GET['editItemId'])) {
        editItem($conn, $_GET['editItemId'], $toDoItem, $isDone);
        header("Location: toDoList.php");
    }
}

// Function to update item then kick back to the toDoList.php page
    function editItem($conn, $itemId, $toDoItem, $isDone) {
        $update = "UPDATE items 
        SET toDoItem=:toDoItem, isComplete=:isDone
        WHERE itemId=:itemId";
        $stmt = $conn->prepare($update);
        $stmt->bindParam(':toDoItem', $toDoItem);
        $stmt->bindParam(':isDone', $isDone, PDO::PARAM_BOOL);
        $stmt->bindParam(':itemId', $itemId);
        $stmt->execute();
    }

    function getValueOfToDoItem($conn, $itemId) {
        $itemValue = "SELECT toDoItem 
            FROM items 
            WHERE itemId=:itemId";
        $stmt = $conn->prepare($itemValue);
        $stmt->bindParam(':itemId', $itemId);
        $stmt->execute();
        
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        foreach($stmt->fetchAll() as $listRow) {
            // echo "<div class='toDoListItem row'>";
            $item = $listRow['toDoItem'];
            echo $item;
        }
    }

?>