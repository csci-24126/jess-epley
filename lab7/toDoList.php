<?php
include("../cms/includes/navbar.php");
?>

<link rel="stylesheet" href="toDoList.css">

<!-- HTML code for our to do list form -->
<div class='toDoList'>
  <form method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]);?>">
    <label for="toDo">To Do:</label>
    <input type="text" name="toDo" id="toDo"><br>

    <input type="checkbox" name="isDone" id="isDone" value="true">
    <label for="isDone">Is Done</label>

    <input type="submit" class="btn btn-primary" value="Submit">
  </form>

<?php 

// Set variables that'll be used in the form
$toDoItem = "";
$isDone = false;

// Accepting user input from the form
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (clean_input($_POST['toDo'])) {
        $toDoItem = clean_input($_POST['toDo']);
    }
    if (isset($_POST['isDone'])) {
        $isDone = true;
    }
}

$conn = connect_to_db("toDoList");

// Calling our functions to allow user to add items to the To Do List
if (!empty($toDoItem)) {
    addToDoListItem($conn, $toDoItem, $isDone);
}

// Function called to allow user to complete and delete an item from the To Do List
if(isset($_GET['completedItemId'])) {
    completeToDoListItem($conn, $_GET['completedItemId']);
} elseif (isset($_GET['deletedItemId'])) {
    deleteToDoListItem($conn, $_GET['deletedItemId']);
} elseif(isset($_GET['editItemId'])) {
    editItem($conn, $_GET['editItemId']);
}

// Function called to print the to do list
printToDoList($conn);



// Function to add an item to the To Do List
function addToDoListItem($conn, $item, $isDone) {
    $insert = "INSERT INTO items (toDoItem,isComplete)
    VALUES (:item, :isDone)";
    $stmt = $conn->prepare($insert);
    $stmt->bindParam(':item', $item);
    $stmt->bindParam(':isDone', $isDone, PDO::PARAM_BOOL);
    $stmt->execute();
}

// Function to print out the To Do List item
function printToDoList($conn) {
    $selectItem = "SELECT * FROM items";
    $stmt = $conn->prepare($selectItem);
    $stmt->execute();
 
    $stmt->setFetchMode(PDO::FETCH_ASSOC);
    foreach($stmt->fetchAll() as $listRow) {
        echo "<div class='toDoListItem row'>";
        $itemId = $listRow['itemId'];
        $item = $listRow['toDoItem'];
        $complete = $listRow['isComplete'] ? 'Done' : 'To Do';
        echo "<p class='col-4 offset-1'>$item</p>";
        if ($complete === "To Do") {
            echo "
            <p class='col-2'>$complete</p>
            <a class='btn btn-success col-1' href='editItem.php?editItemId=$itemId'>Edit</a>
            <a class='btn btn-danger offset-1 col-1' href='toDoList.php?deletedItemId=$itemId'>Delete</a> </br>";
        } else {
            echo "<p class='col-2'>$complete</p>
            <a class='btn btn-danger offset-2 col-1' href='toDoList.php?deletedItemId=$itemId'>Delete</a> </br>";
        }        
        echo "</div>";
    }
}

// Function for completing an item on the To Do List
function completeToDoListItem($conn, $itemId) {
    $update = "UPDATE items
        SET isComplete = true
        WHERE itemId=:itemId";
    $stmt = $conn->prepare($update);
    $stmt->bindParam(':itemId', $itemId);
    $stmt->execute();
}

// Function for deleting a completed item on the To Do List
function deleteToDoListItem($conn, $itemId) {
    $delete = "DELETE FROM items WHERE itemId=:itemId";
    $stmt = $conn->prepare($delete);
    $stmt->bindParam(':itemId', $itemId);
    $stmt->execute();
}
