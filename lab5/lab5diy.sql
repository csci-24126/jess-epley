USE lab5;

DROP TABLE IF EXISTS birthdays;

CREATE TABLE birthdays(
primaryKey INT AUTO_INCREMENT PRIMARY KEY,
firstName VARCHAR(255),
familyMember BOOL,
birthday DATE,
favoriteColor VARCHAR(255)
);

INSERT INTO birthdays(firstName, familyMember, birthday, favoriteColor)
VALUES
("Jess", true, "1900-04-07", "Green");

INSERT INTO birthdays(firstName, familyMember, birthday, favoriteColor)
VALUES
("JP", false, "1900-07-18", "Green");

INSERT INTO birthdays(firstName, familyMember, birthday, favoriteColor)
VALUES
("Madison", true, "1900-01-15", "Black");

INSERT INTO birthdays(firstName, familyMember, birthday, favoriteColor)
VALUES
("Whitney", true, "1900-08-01", "Red");

INSERT INTO birthdays(firstName, familyMember, birthday, favoriteColor)
VALUES
("Natalie", false, "1900-04-21", "Purple");

SELECT * FROM birthdays;