DROP TABLE IF EXISTS movies;

USE lab5;

CREATE TABLE movies(
primaryKey INT AUTO_INCREMENT PRIMARY KEY,
title VARCHAR(255),
director VARCHAR(255),
releaseDate DATE);

INSERT INTO movies (title, director, releaseDate)
VALUES 
("The Matrix", "The Wachowskis", "1999-05-31");

INSERT INTO movies (title, director, releaseDate)
VALUES 
("The Matrix Reloaded", "The Wachowskis", "2003-05-15");

SELECT * FROM movies;