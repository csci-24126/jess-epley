<?php

//  My hobby for this exercise will be Diablo 3 related

$item = "Chest Armor";
$socketsTotal = 3;
$armorRating = 750;
$dexterityRating = 833;
$isLegendary = true;

$testNumber = 19;
if ($testNumber % 5 == 0 && $testNumber % 3 == 0) {
    echo "divisible by both 3 & 5";
} elseif ($testNumber % 5 == 0) {
    echo "divisible by 5";
} elseif ($testNumber % 3 == 0) {
    echo "divisible by 3";
} else {
    echo "not divisible by 3 or 5";
}