<?php
	// echo "Hello world!";
$firstName = "Jess";
$lastName = "Epley";
$age = 31;
$currentYear = 2022;
$isMarried = true;

echo "$firstName $lastName </br>";
echo $firstName . " freaking " . $lastName . "</br>";
echo "I was born in " . $currentYear - $age . "</br>";
echo "I am married: $isMarried </br>";

$generation = "";
$birthYear = 1990;
if ($birthYear < 1965) {
    $generation = "Boomer";
} elseif ($birthYear >= 1965 && $birthYear < 1981) {
    $generation = "Gen X";
} elseif ($birthYear >= 1981 && $birthYear < 1996) {
    $generation = "Millennial";
} elseif ($birthYear >= 1996 && $birthYear < 2012) {
    $generation = "Gen Z";
} else {
    $generation = "Too young for a generational label";
}
echo $generation ."</br>";
