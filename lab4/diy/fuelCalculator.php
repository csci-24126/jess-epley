<html>
    <head>
    <link rel="stylesheet" href="styles.css">
    </head>
<body>
    <div id="mainpage">
    <h1>Welcome to the Fuel Calculator!</h1>
    <h2>This tool will help you budget for the weekly cost of gas.</h2>

    <?php

    // Initializing variables used for form input and calculating fuel cost.
    $avgMilesDriven = $userCarMPG = $fuelPerks = 0;
    $fuelCostPerGallon = "";
    $totalPerksDiscount = "";
    $totalGasCost = "";

    // Variables to catch input errors
    $avgMilesErr = "";
    $mpgErr ="";
    $perksErr = "";

    if ($_SERVER["REQUEST_METHOD"] == "POST") {

        if (!is_numeric($_POST["avgMilesDriven"])) {
                $avgMilesErr = "Please enter a valid number";

            } elseif (empty($_POST["avgMilesDriven"])) {
                $avgMilesErr = "Please enter miles driven in a week.";
            }
            else {
            $avgMilesDriven = clean_input($_POST["avgMilesDriven"]); }

        if (!is_numeric($_POST["userCarMPG"])) {
                $mpgErr = "Please enter a valid number";
            } elseif (empty($_POST["userCarMPG"])) {
                $avgMilesErr = "Please enter you car's average MPG.";
            } else {
            $userCarMPG = clean_input($_POST["userCarMPG"]); }

        if (!is_numeric($_POST["fuelPerks"]) && !empty($_POST["fuelPerks"])) {
                $perksErr = "Please enter a valid number";
            } else {
            $fuelPerks = clean_input($_POST["fuelPerks"]); }
        
        $fuelCostPerGallon = $_POST["fuelType"];
        $totalPerksDiscount = perksDiscount($fuelPerks);

        if ($avgMilesDriven != 0 && $userCarMPG != 0) {
        $totalGasCost = totalFuelCost($avgMilesDriven, $userCarMPG, $fuelCostPerGallon, $totalPerksDiscount);
        }
    }

    // Function to address malicious / erroneous input
    function clean_input($data) {
        $data = trim($data); // removes whitespace
        $data = stripslashes($data); // strips slashes
        $data = htmlspecialchars($data); // replaces html chars
        return $data;
    }
    
    // Calculating fuel perks discount given the conversion provided
    function perksDiscount($fuelPerks) {
        if ($fuelPerks) {
            $totalPerksDiscount = (floor($fuelPerks / 100)) * 0.10;
        } else {
            $totalPerksDiscount = 0;
        }
        return $totalPerksDiscount;
    }

    // Calculating the total gas cost based on all info retrieved by the form
    function totalFuelCost($avgMilesDriven, $userCarMPG, $fuelCostPerGallon, $totalPerksDiscount) {
       
        // Getting radio button selection to calculate total fuel cost
        
        $totalGasCost = ($avgMilesDriven / $userCarMPG) * ($fuelCostPerGallon - $totalPerksDiscount);
        $totalGasCost = number_format($totalGasCost, 2, '.', '');
        // echo "$totalGasCost <br>";
        // echo "$avgMilesDriven<br>";
        // echo "$userCarMPG<br>";
        // echo "$fuelCostPerGallon<br>";
        // echo "$totalPerksDiscount<br>";
        return $totalGasCost;
    }

    ?>

    <!-- User input form to get data for calculator -->
    <form action="<?php htmlspecialchars($_SERVER["PHP_SELF"])?>" method="post">

        <!-- Getting user input for their average miles driven / their car's average MPG -->
        <label for="avgMilesDriven"><strong>Average miles driven per week: </strong></label>
        <input type="text" name="avgMilesDriven" id="avgMilesDriven" required>
        <span class="error"> * <?php echo $avgMilesErr;?></span>
        <br><br>
        <label for="userCarMPG"><strong>Your car's average MPG: </strong></label>
        <input type="text" name="userCarMPG" id="userCarMPG" required>
        <span class="error"> * <?php echo $mpgErr;?></span>
        <br><br>


        <!-- Radio buttons to select fuel type -->
        <p><strong>Select a fuel type: </strong><span class="error"> *</span></p> 
        <input type="radio" name="fuelType" id="octane87" value="1.89" checked>
        <label for="octane87">87 octane - $1.89 per gallon</label><br>
        <input type="radio" name="fuelType" id="octane89" value="1.99">
        <label for="octane89">89 octane - $1.99 per gallon</label><br>
        <input type="radio" name="fuelType" id="octane92" value="2.09">
        <label for="octane92">92 octane - $2.09 per gallon</label>

        <br><br>

        <!-- Optional entry by user for their fuel perks discount -->
        <label for="fuelPerks"><strong>Amount spent on groceries per week: </strong></label>
        <input type="text" name="fuelPerks" id="fuelPerks">
        <span class="error"><?php echo $perksErr;?></span>
        <br><br>

        <input type="submit" value="Submit" name="Submit">
        <p><span class="error">* required fields</span></p>
    </form>
    
    <!-- Display results to user -->
    <h1>Here are your results...</h1>
        <h3>Your average miles driven is: <?php echo $avgMilesDriven; ?>.<br>
         Your car's average MPG is: <?php echo $userCarMPG; ?>.<br>
         Your fuel cost per gallon is: $<?php echo $fuelCostPerGallon; ?>.<br>
         Your fuel perks discount is: $<?php echo $totalPerksDiscount; ?>.<br>
         Your weekly cost for gas is $<?php echo $totalGasCost; ?>.</h3><br>
</div>
</body>
</html>
