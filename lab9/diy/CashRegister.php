<?php
class CashRegister {
    // Properties
    protected $amountInRegister;

    // Constructor
    function __construct($amountInRegister) {
        $this->amountInRegister = $amountInRegister;
    }

    // Destructor
    function __destruct() {    }

    // Setter & Getter
    function set_amountInRegister($amountInRegister) {
        $this->amountInRegister = $amountInRegister;
    }
    function get_amountInRegister() {
        return $this->amountInRegister;
    }

    // Methods
    function addMoney($addedAmount) {
        $this->amountInRegister = $this->amountInRegister + $addedAmount;
        return $this->amountInRegister;
    }

    function removeMoney($removedAmount) {
        if($removedAmount > $this->amountInRegister) {
            echo "You can't remove that much money from the register!</br>";
        } else {
        $this->amountInRegister = $this->amountInRegister - $removedAmount;
        return $this->amountInRegister;
        }
    }
}

// Testing the CashRegister object

// $testRegister = new CashRegister(50);
// $testRegister->addMoney(75);
// print_r($testRegister);

// $testRegister->removeMoney(100);
// print_r($testRegister);

// $testRegister->removeMoney(1000);