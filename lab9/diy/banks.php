<?php
include("PiggyBank.php");

// Testing our PiggyBank object, and printing the value within the PiggyBank.
$piggyBank = new PiggyBank(20); // Should be 20
print_r($piggyBank);
echo "</br>";

$piggyBank->addMoney(10); // Should be 30
print_r($piggyBank);
echo "</br>";

$piggyBank->removeMoney(5); // Can't perform this on PiggyBank
print_r($piggyBank);
echo "</br>";

$piggyBank->removeMoney(70); // Can't perform this on PiggyBank
print_r($piggyBank);
echo "</br>";

$piggyBank->breakBank(); // Smash the bank, telling how much was inside, then reset value to ZERO.
print_r($piggyBank);
echo "</br>";

echo "</br>"; // Whitespace just to make the results page look a little more coherent

$cashRegister = new CashRegister(20); // Should be 20
print_r($cashRegister);
echo "</br>";

$cashRegister->addMoney(10); // Should be 30
print_r($cashRegister);
echo "</br>";

$cashRegister->removeMoney(5); // Should be 25
print_r($cashRegister);
echo "</br>";

$cashRegister->removeMoney(70); // Should not be possible
print_r($cashRegister);
echo "</br>";