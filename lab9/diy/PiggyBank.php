<?php
include("CashRegister.php"); // Including parent class to create inheritance

class PiggyBank extends CashRegister {

    // Methods
    function removeMoney($removedAmount) {
        echo "You can't remove money from the Piggy Bank without smashing it!</br>";
    }
    function breakBank() {
        echo "We smashed the Piggy Bank and removed $" . $this->amountInRegister . " from inside.</br>";
        $this->set_amountInRegister(0);
    }
}

// Testing the PiggyBank object

// $testPiggyBank = new PiggyBank(100);
// $testPiggyBank->removeMoney(10);
// $testPiggyBank->breakBank();
// print_r($testPiggyBank->get_amountInRegister());