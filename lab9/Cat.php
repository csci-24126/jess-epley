<?php
include("Animal.php");

class Cat extends Animal {
    public function amountOfTimeSpentSleeping() {
        $ageInDays = $this->age * 365; // convert age from years to days
        $timeSpentSleeping = $ageInDays * 15; // multiply days by how many hours per day spent sleeping
        echo "$this->name has spent $timeSpentSleeping hours sleeping.</br>";
    }
}

$lulu = new Cat("Lulu", 5);
$lulu->amountOfTimeSpentSleeping();
?>