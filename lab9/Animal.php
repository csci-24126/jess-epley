<?php
class Animal {
    // Properties
    public $name;
    public $age;

    // Constructor
    function __construct($name, $age) {
        $this->name = $name;
        $this->age = $age;
    }

    // Destructor
    function __destruct() {
        echo "$this->name is no longer being used, so it's getting destroy </br>";
    }

    // Getters & Setters
    function set_name($name) {
        $this->name = $name;
    }
    function get_name() {
        return $this->name;
    }

    function set_age($age) {
        $this->age = $age;
    }
    function get_age() {
        return $this->age;
    }

    // Additional methods
    function echoNameAndAge() {
        echo "$this->name is $this->age years old </br>";
    }
}

// // Creating new Animal objects and practicing setting / getting the properties of that object
// $fido = new Animal("Fido", 100);
// echo $fido->get_name() . "</br>";
// echo $fido->get_age() . "</br>"; 
// $fido->echoNameAndAge();

// $morty = new Animal("Morty", 3);
// $morty->echoNameAndAge();

// $mrMeowgi = new Animal("Mr. Meowgi", 11);
// $mrMeowgi->echoNameAndAge();

// $spot = new Animal("Spot", 3);
// $spot->echoNameAndAge();

?>