<?php
include("Animal.php");

// Practicing inheritance by extending a parent class's properties to a child class
class Dog extends Animal {
    public function convertAge() {
        $convertedAge = $this->age * 7;
        echo "The dog $this->name is $this->age years old, which is $convertedAge dog years!</br>";
    }
}

$pochi = new Dog('Pochi', 7);
$pochi->convertAge();
?>